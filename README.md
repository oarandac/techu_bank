# TechU Bank

Este es el proyecto final del curso Practitioner dentro de Tech University.
Autor: [Oscar Aranda (oscar.aranda@bbva.com)](mailto:oscar.aranda@bbva.com)


## Diseño de la aplicación

La aplicación se compone de dos partes diferenciadas:

- Backend (API). Se trata de un API desarrollado sobre nodejs con expressjs. Este API se utiliza una base de datos no relacional de tipo MongoDB (alojada en MLAB - DBaaS) 

    [Documentación detallada de la parte backend](README.back.md)

- Frontend. Aplicacion HTML/JS/CSS basada en Web Components -  [Polymer 2](https://www.polymer-project.org/)

    [Documentación detallada de la parte frontend](README.front.md)

La aplicación se apoya en una serie de APIs externas (S3, GCP, etc) que se acceden desde la parte back o front

### Visión general de la aplicación

En el siguiente esquema se puede ver las distintas piezas que forman la aplicación

![Diseño general](https://s3-eu-west-1.amazonaws.com/techu-bank-doc/schema.png)


