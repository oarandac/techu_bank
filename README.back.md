# TechU Bank

Este es el proyecto final del curso Practitioner dentro de Tech University.
Autor: [Oscar Aranda (oscar.aranda@bbva.com)](mailto:oscar.aranda@bbva.com)

---

## Índice

Puntos incluidos en este documento.

* Repositorio
* Seguridad
* AI
* MongoDB
* API
* Testing
* Configuración
* Docker 
* Openshift

---

## Repositorio

El código fuente del proyecto se encuentra alojado en BitBucket. 

https://oarandac@bitbucket.org/oarandac/techu_bank.git

1. Clonar el repositorio
```js
git clone https://oarandac@bitbucket.org/oarandac/techu_bank.git
```

2. Cambiar al diretorio `back`
```js
cd back
```

3. Instalar dependencias
```js 
npm install
```

4. Arrancar servidor de desarrollo (nodemon)
```js
npm start
```

5. Arrancar servidor de desarrollo (nodejs)
```js
node server.js
```

6. Ejecutar test 
```js
npm test
```

**NOTA IMPORTANTE**: En el repositorio no se han incluido API KEYS ya que había que hacerlo público. En [este documento](https://docs.google.com/presentation/d/19GIGvOLuT7WSdLrH3aWOIGQeaFTjpp3A1aVpW7iM2zY/edit?usp=sharing) se indica cómo configurar las claves para probar el proyecto en local

---

## Seguridad

Medidas de seguridad implementadas en el proyecto

1. Contraseñas. Las contraseñas no se encuentran almacenadas en plano en MongoDB, sino que se almacena su hash. En el proceso de login, se realiza la validación del hash. Para esto, se ha utilizado la [librería bcrypt](https://www.npmjs.com/package/bcrypt)
2. Token JWT. Una vez validada la contraseña, se retorna un token JWT en una cabecera de la respuesta en la llamada a /login. Para generar el token se utiliza la librería [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken). Este token, debe ser incluido en la cabecera 'token' en la petición al resto de endpoints que forman el API. La validación del token se hace mediante el método validate del módulo token. Este método se ha implentando como middleware de express. 
3. Driver. Para la conexión a la BBDD, se ha optado por utilizar el driver [MongoDB para NodeJs](http://mongodb.github.io/node-mongodb-native/) en lugar del API REST

4. Validacion de parametros de entrada. La validacion de los parámetros de entrada se hace mediante el módulo [express-validations](https://github.com/AndrewKeig/express-validation). La implementación se hace en la forma de middleware de express y para cada servicio se define un objeto con las validaciones a implementar. Estas validaciones se encuentran en el fichero `validations/validations.js`. Ejemplo de un objeto validation:
```javascript
const post_transaction = {
  headers: {
    token: Joi.string().required(),
  },
  params: {
    id: Joi.number().integer().positive().required()
  },
  body: {
    description: Joi.string().regex(/[a-zA-Z ]/).required(),
    amount: Joi.number().precision(2).required();
    type: Joi.string().valid('deposit','payment','transfer').required(), 
    destination: Joi.number().integer().min(1).optional()
  }
};
```

5. CORS. Se ha utilizado la librería [CORS](https://github.com/expressjs/cors) de expressjs para habilitar CORS y por tanto permitir las llamadas al API desde páginas servidas desde dominios distintos al de la propia API. Como el token de autenticación se retorna en una cabecera de la respuesta, se ha tenido que incluir la cabecera token en el 'Access-Control-Expose-Headers'
  ```javascript
  var corsOptions = {
      'allowedHeaders': ['token', 'Content-Type'],
      'exposedHeaders': ['token'],
      'origin': '*',
      'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
      'preflightContinue': false
  };
  ```
---
## Inteligencia Artificial
Se ha incluido la funcionalidad de *feedback* de usuario en la aplicación. Mediante esta funcionalidad, el usuario puede dar su opinión sobre la aplicación. Esta opinión, se analiza mediante el servicio [Cloud Natural Language de Google](https://cloud.google.com/natural-language/) para detectar el sentimiento del usuario y retornar un mensaje de respuesta adaptado a la opinión enviada. Este servicio, retorna un parámetro 'score' cuyo valor oscila entre -1 y 1 (-1 valoración muy negativa y +1 valoración muy positiva). En función de este valor se dan tres mensajes de respuesta

| Score        | Respuesta           | 
| ------------ |:-------------:| 
| x < -0.2     | Sentimos mucho que tu experiencia no haya sido satisfactoria. Seguiremos trabajando en mejorar la aplicación |
|  -0.2 < x < 0.2 | Seguiremos trabajando para que la experiencia de usuario sea mejor cada día |
| x > 0.2 | Nos alegra escuchar que te ha gustado nuestra aplicación. Muchas gracias por tus palabras |

Se ha utilizado la librería cliente para NodeJS y se ha creado el servicio `/feedback` para acceder a la funcionalidad desde el front. 

Nota: este servicio de Google no dispone de cliente Javascript, sólo cliente NodeJS. Por este motivo se ha incluido en la parte back. 

Nota2: Para la autenticación del API, y dado que su uso puede implicar costes, el fichero de credenciales no se ha subido al repositorio BitBucket. Se han incluido instrucciones en [este documento](https://docs.google.com/presentation/d/19GIGvOLuT7WSdLrH3aWOIGQeaFTjpp3A1aVpW7iM2zY/edit?usp=sharing) para poder habilitar la api key de este servicio

---
## MongoDB
Los datos de la aplicación se almacenan en una base de datos no relacional - MongoDB - alojada en [MLAB](https://mlab.com/) como un DBaaS. 
Se han creado tres colecciones: users, accounts y transactions. A continuación se detallan las características principales de cada colección.  
### Users 
Datos de los usuarios/clientes del banco. El campo contraseña contiene el hash de la contraseña.
Documento de ejemplo:
```json
 {
    "id": 3,
    "first_name": "Danyette",
    "last_name": "Yurasov",
    "email": "dyurasov2@deviantart.com",
    "country": "PL",
    "password": "$2a$10$sndkyiUz6H2ZHkH66PJKX.2ZbD1RcDztgKrXTfPycsLHnWBsQxPdS"
  }
```
**Índices**. Se han creado dos índices. Uno para el campo 'email' con la restricción 'unique' utilizado en la operativa de login. Esto mejora el rendimiento de las búsquedas por email (login) y evita que dos usuarios se registren con el mismo email. 
```javascript
  db.users.ensureIndex({'email':1},{unique: true})
```
Por otro lado, se ha creado un segundo índice por el campo 'id' en orden descendiente para optimizar la obtención del último id de usuario asignado (operativa de alta de nuevo usuario). Este índice tambien se ha creado 'unique' para no permitir clientes con el mismo 'id'
```javascript
  db.users.ensureIndex({'id':-1},{unique: true})
```
### Accounts
La colección 'accounts' representa las cuentas bancarias. El campo 'clients' contiene un array de ids de clientes propietarios de la cuenta (una cuenta puede tener mas de un titular)

Documento de ejemplo:

```json
 {
    "id": 1,
    "iban": "ES018200998899899998",
    "type": "checking",
    "balance": 3123.12,
    "currency": "EUR",
    "alias": "my checking account",
    "clients": [
        2,
        3
    ]
  }
```

**Índices**. Se ha creado un indice compuesto por los campos 'id' y 'clients'. Este índice es necesario ya que se realizan consultas seleccionando una cuenta por id y por cliente 'propietario de la cuenta'
```javascript
  db.accounts.ensureIndex({'id':1, 'clients':1})
```
### Transactions

La coleccion 'transactions' almacena los movimientos de las cuentas. Tiene una clave externa 'account_id' con el campo 'id' de la colleción 'accounts' (1->N)
```json
  {
    "account_id": 1,
    "amount": -20,
    "description": "gas bill",
    "transaction_id": "rc098po6o0",
    "date": "2016-01-31T10:12:54.012Z"
  }
```
**Índices**. Se ha creado un indice por los campos 'account_id' y date, ya que las consultas se filtran por account_id y se ordenan por fecha (descendente)
```javascript
  db.transactions.ensureIndex({'account_id':1, 'date':-1})
```
---

## API

A continuación se documenta el API expuesta. 

* **GET /techu_bank/V1/ping**

  Endpoint para validar el despliegue correcto del API

* **POST /techu_bank/V1/login**

  Fuente: [`controller/users.js`](back/controller/users.js)

  Realiza login de un usuario. Retorna token jwt que deberá ser enviado en el resto de peticiones del API. Esta acción no realiza ninguna acción sobre la base de datos. 
  * Input. 
    * Body. JSON
      * `email`
      * `password`
  * Output. 
    * Header
      * `token`. [Cabecera HTTP]. Token JWT
    * Body. JSON
      * user
        * `id` 
        * `first_name` 
        * `last_name`
        * `email`
        * `country`

* **POST /techu_bank/V1/users**

  Fuente: [`controller/users.js`](back/controller/users.js)

  Crea un nuevo usuario. Además de nuevo usuario, se crea una cuenta con saldo 0. 
  * Input.
    * Body. JSON
      * `first_name`. Nombre (Obligatorio)
      * `last_name`. Apellidos (Obligatorio)
      * `email`. Email. (Obligatorio)
      * `password`. Contraseña. (Obligatorio)
      * `country`. Codigo de país. Dos posiciones (Opcional)

* **GET  /techu_bank/V1/accounts**

  Fuente: [`controller/accounts.js`](back/controller/accounts.js)

  Retorna la lista de cuentas de un usuario. El usuario se obtiene a partir del token.
  * Input.
    * Header. JSON
      * `token`
    * Query params
      * limit (opcional). Paginación. tamaño máximo de la respuesta
      * skip. (opcional). Paginación. Indice de resultados a devolver
  * Output. 
      * Body. JSON
        - Array. Objetos Account. 
        ```
          {
            "_id": "5ab75604f36d282750937985",
            "id": 1,
            "iban": "ES018200998899899998",
            "type": "checking",
            "balance": 3123.12,
            "currency": "EUR",
            "alias": "my checking account",
            "clients": [
                2,
                3
            ]
          }
          ```


* **GET  /techu_bank/V1/accounts/XXXX/transactions**

  Fuente: [`controller/transactions.js`](back/controller/transactions.js)

  Retorna la lista de movimientos de una cuenta. 
  * Input
    * Header. JSON. 
      * `token`. Token JWT
    * Param
      * accountId. Campo 'id' de la cuenta (no el '_id' de mongoDB)
    * Query params
      * limit (opcional). Paginación. tamaño máximo de la respuesta
      * skip. (opcional). Paginación. Indice de resultados a devolver
  * Output
    * Body. JSON
      * Array de objetos Transaction
      ```js
      [
      {
        "_id": "5abfee1a99d78b79539e13f5",
        "account_id": 1,
        "amount": -60.76,
        "description": "moe's tabern",
        "trasaction_id": "8m49qkdb4k",
        "date": "2018-03-31T20:22:50.108Z"
      },
      ...
      ]
      ```
* **POST /techu_bank/V1/accounts/XXXXXXXXX/transactions**

  Fuente: [`controller/transactions.js`](back/controller/transactions.js)

  Crea un nuevo movimiento
  * Input
    * Header. JSON
      * `token`. JWT token
    * Param
      - accountId. Campo 'id' de la cuenta (no el '_id' de mongoDB)
    * Body. Objeto transaction
    ```js
      {
        "type": "deposit"|"payment"|"transfer",
        "amount": 100.20,
        "description": "descripcion del movimiento",
        "destination": "3" //(cuenta destino en caso de transferencias - Opcional)
      }
    ```
  * Output

    * HTTP Code: 201 Created

* **POST /techu_bank/V1/feedback**
  
  Fuente: [`controller/feedback.js`](back/controller/feedback.js)

  Envia el comentario de un cliente. 
  Nota: El comentario no se almacena. Este servicio es sólo para probar API con `GCP IA`

  * Input
    * Header. JSON
      * `token`. JWT token
    * Body. Texto e idioma 
    ```js
      {
        "text": "Enhorabuena. Me encanta la aplicación",
        "lang": "es"
      }
    ```
  * Output
    * Body. Texto de agradecimiento y score calculado
    ```js
     { 
      response_text: "Nos alegra escuchar que te ha gustado nuestra aplicación. Muchas gracias por tus palabras",
      score: 0.89987
     }
     ```



---
## Testing

Se ha implementado una batería de test para probar la funcionalidad del api. Los test se encuentran en la carpeta /test y se lanzan mediante el comando `npm test`

Se ha utizado [mocha](https://mochajs.org/) como _framework de test_ junto con [Chai](http://www.chaijs.com/)/[Chai-http](https://github.com/chaijs/chai-http) como librerías de _assertions_ 

__Importante:__ Para las pruebas se utiliza una base de datos independiente (*techu_bank_test*) para no interferir en los datos de los entornos de develop y production. En cada ejecución, se cargan en esta bbdd de pruebas los datos de las colecciones necesarias (método `before()`). A la finalizacion de los test (método `after()`) se vacían las colecciones. Los datos de carga se encuentran en la carpeta `/test-data`

---
## Configuración

La configuración se gestiona con el módulo [config](https://github.com/lorenwest/node-config). Los ficheros de configuración se encuentran en `/config`. La aplicación cuenta con dos entornos: test y default (resto). El entorno de test solo difiere del resto de entornos en la URL de la BBDD mongo (ya que usa su propia BBDD) y el puerto en el que se lanza express (3005)

---
## Docker 

Se ha incluido un fichero Dockerfile para construir una imagen Docker con el servidor express ejecutando la parte backend del proyecto

Construir imagen:
```
sudo docker build -t oarand/techubank_back .
```
Ejecutar imagen (puerto mapeado 3000)
```
sudo docker run -p 3000:3000 -d oarand/techubank_back
```
La imagen se encuentra publicada en [DockerHub (oarand/techubank_back)](https://hub.docker.com/r/oarand/techubank_back/)

## Openshift

La imagen Docker de la parte back del proyecto se ha desplegado en openshift. 

La funcionalidad del API está expuesta en el siguiente endpoint

http://techubankback-techu-bank.7e14.starter-us-west-2.openshiftapps.com/

Se puede validar si el API está corriendo lanzando esta URL

http://techubankback-techu-bank.7e14.starter-us-west-2.openshiftapps.com/techu_bank/v1/ping



