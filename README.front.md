# TechU Bank Front

Este es el proyecto final del curso Practitioner dentro de Tech University
Autor: [Oscar Aranda (oscar.aranda@bbva.com)](mailto:oscar.aranda@bbva.com)

---

## Índice

Puntos incluidos en este documento.

* Repositorio
* Diseño
* Redux
* Router
* Acceso a API's
* Componentes de terceros
* Styling
* APIs externas
* Spinner (velo)
* Scroll infinito
* Build
* Docker
* OpenShift

---

## Repositorio

El código fuente del proyecto se encuentra alojado en BitBucket.

https://oarandac@bitbucket.org/oarandac/techu_bank.git

1.  Clonar el repositorio

    ```
    git clone https://oarandac@bitbucket.org/oarandac/techu_bank.git
    ```

2.  Cambiar al diretorio `front`

    ```
    cd front
    ```

3.  Instalar dependencias

    ```
    bower install
    npm install
    ```

    NOTA: El proyecto incluye componentes del repositorio global de Cells. Para poder descargar estas dependencias es necesario configurar correctamente el acceso al repositorio corporativo (Artifactory). [Más info](https://bbva-devplatform.appspot.com/en-us/developers/engines/cells/documentation/quick-start/introducing-cells#content8)

4.  Arrancar servidor de desarrollo

    ```
    npm run start
    ```

    Este script de npm, ademas de lanzar el `polymer serve` establece la configuración de entorno local.

5.  Construir

    ```
    npm run build
    ```

**NOTA IMPORTANTE**: En el repositorio no se han incluido API KEYS ya que había que hacerlo público. En [este documento](https://docs.google.com/presentation/d/19GIGvOLuT7WSdLrH3aWOIGQeaFTjpp3A1aVpW7iM2zY/edit?usp=sharing) se indica cómo configurar las claves para probar el proyecto en local

---

## Diseño 


La aplicación tiene un componente principal (raiz) [`<techu-bank-app>`](front/src/techu-bank-app.html).

Este componente ráiz a parte de pintar las partes fijas de la aplicacion (cabecera, velo de espera, dialogos de notificación), define la navegación de la aplicacion mediante el componente `<app-router>`, así como declara los data managers encargados de interactuar con el backend para la recuperación de datos. 

---
## Redux

Se ha utilizado redux para gestionar el estado de la aplicación. Para ello, me he apoyado en módulo [`polymer-redux`](https://github.com/tur-nr/polymer-redux) que permite la utilización de redux dentro de aplicacion Polymer. 

El estado de la aplicación lo forman las siguientes propiedades 

```json
{
    token: null,
    user: {},
    accounts: [],
    selectedAccount: 0,
    movements: [],
    moreMovements: true,
    newUser: {},
    newTransaction: {},
    weather_info: {},
    appconfig: {}
    }
```

- token. Token para la autenticación en las llamadas al API. Este token se establece en el proceso de login y se inyecta en las cabeceras HTTP en las peticiones al API.
- user. Objecto con datos del usuario logado
- accounts. Array con las cuentas del usuario logado
- selectedAccount. Id de la cuenta que selecciona el usuario
- movements. Lista de movimientos de la cuenta seleccionada.
- moreMovements. Indica si la cuenta de la que se están visualizando los movimientos tiene más movimientos (para activar/desactivar la paginación)
- newUser. Datos del usuario que se esta realizando el proceso de registro
- newTransaction. Datos del nuevo movimiento que está dando de alta el usuario. 
- weather_info. Datos de la información meteorológica del lugar desde donde se esta conectando el usuario y que se muestra cuando el usuario completa el logon. 
- appconfig. Datos con la configuración de la aplicación. Este objeto se alimenta de un [fichero JS](front/js/config.js) 

Todos los componentes que interactuan con algún dato del estado de la aplicacion (bien porque lean datos o bien porque lancen alguna acción que lo modifique) heredan de una de estas dos clases:

- [ReduxMixin](front/src/redux-mixin.html). Clase base que define el Store y el Reducer. 

- [ReduxDmMixin](front/src/redux-dm-mixin.html). Clase que hereda de ReduxMixin, y que además define las propiedades más comunes (appconfig, token y user) utilizadas por los data managers (DM).

El siguiente esquema muestra la jerarquía de clases definida para los componentes que forman la aplicación. 

![ClasesRedux](https://s3-eu-west-1.amazonaws.com/techu-bank-doc/redux.png)


Cada elemento de la aplicacion, define las acciones que dispara. Ejemplo:

```javascript
static get actions() {
    return {
        setToken(token) {
            return {
                type: 'SET_TOKEN',
                token
            };
        }
    }
}
```

## Router

Se ha utilizado `<app-router>` para gestionar la navegacion entre las distintas partes de la aplicación. 
Cada ruta realiza el `ìmport` del componente padre de cada funcionalidad. 

La aplicación contiene definidas las siguientes rutas:

| Ruta                 | import            |
| -------------------- | ----------------- |
| /                    | techu-home        |
| /login               | techu-login       |
| /dashboard           | techu-dashboard   |
| /movements/:accid    | techu-movements   |
| /transactions/:accId | techu-transaction-form |
| /enroll              | techu-enrol-form       |
| \*                   | techu-not-found   |

Se ha diseñado la aplicacion para que cada ruta pueda ser recargada (F5). Para que no se pierda el logon del usuario, tanto el token como los datos del usuario se almacenan en el session storage del navegador.

Para ello se ha utilizado el componente `<app-localstorage-document>`. 

Al recargar la aplicacion se comprueba si session storage tiene informacion de usuario logado (token + user). Si la tiene, la establece en el Store mediante las acciones SET_TOKEN y SET_USER. 

---
## Acceso a APIS

Tanto para el acceso al API propia, como a APIS externas, se han creado componentes con el sufijo DM (Data Manager). 
Las llamadas XHR se hacen mediante `<iron-ajax>`

```html
<iron-ajax id="ajax" method="GET" url="[[appconfig.api_endpoint]]/accounts" content-type="application/json" handle-as="json"
      on-response="_handleResponse" on-error="_handleError" bubbles>
    </iron-ajax>
```

## Componentes de terceros

Se han reutilizado componentes polymer disponibles en varios catálogos 

### [Cells Catalog](https://bbva-ether-cellscatalogs.appspot.com/?version=v200#/)

- `cells-credentials-form`. Formulario para que el usuario introduzca sus credenciales y se autentique
- `cells-molecule-spinner-veil`. Velo (capa de espera )
- `cells-molecule-input`. Caja de texto (input)
- `cells-select`. Lista desplegable de valores
- `cells-st-button`. Botón
- `coronita-icons`. Colección de iconos
- `cells-radio-group`. Radio buttons

### [webcomponents.org](https://www.webcomponents.org/)
- `vaadin-notification`. Panel de notificaciones. 
- `yo-file-uploader`. Elemento que permite seleccionar un fichero mediante drag and drop. Usado para adjuntar un avatar en el proceso de registro
- `paper-dialog`. Dialogos modales. Usado para funcionalidad de feedback.
- `paper-avatar`. Muestra el avatar del usuario en la cabecera de la aplicación. 
- `paper-tooltip`. Añade tooltip a otros elementos visuales. 
- `app-localstorage-document`. Acceso a localStorage donde se almacenan token y datos de usuario logado.

---

## Styling 

### Bootstrap

Para dar un toque moderno al diseño de la aplicación, nos hemos apoyado en la librería [Boostrap](https://getbootstrap.com/). Para su utilización en componentes Polymer, hemos incluido la dependencia con [granite-bootstrap](https://github.com/LostInBrittany/granite-bootstrap), que expone Boostrap como [shared-styles](https://www.polymer-project.org/2.0/docs/devguide/style-shadow-dom#share-styles-between-elements) de Polymer.

```html
<link rel="import" href="../bower_components/granite-bootstrap/granite-bootstrap.html">

<dom-module id="techu-feedback-form">
  <template>
    <style include="granite-bootstrap"></style>
    ...
  </template>
</dom-module>
```

### Estilos propios

Se ha creado un componente para establecer los estilos comunes entre los distintos elementos de la aplicación

El componente es [`techu-styles`](front/src/techu-styles.html)

### Responsive

Se han incluido algunos estilos que tienen en cuenta el tamaño del viewport del dispositivo para que los distintos elementos puedan verse correctamente en dispositivos móviles

```css
@media (min-width:768px) {
    .techuTitle {
        font-size: 3em;
        font-weight: 300;
    }
    .techuTitle2 {
        font-size: 2em;
        font-weight: 300;
    }
    .techuTitle3 {
        font-size: 1.5em;
        font-weight: 300;
    }
    .displayIban {
        max-width: 350px;
    }
}
```

---

## APIs externas

En el proyecto se han utilizado cuatro APIS externas:

###  **Feedback**

Se ofrece la posibilidad de que el usuario escriba su opinión sobre la aplicación. El comentario se envía a un servicio de reconocimiento de lenguaje natural para detectar el sentimiento del usuario (bueno, malo, regular) y ofrece una respuesta personalizada. Este API le llama desde la parte back de la aplicacion ya que Google por el momento no ofrece API Web. Para más información consultar la documentación de la parte de [backend](./README.back.md)

###  **Amazon S3**

Cada usuario, tiene asociado un avatar (imagen de perfil). Esta imagen se almacena en Amazon S3, en el bucket `techu-bank` (arn:aws:s3:::techu-bank).
La subida a S3 se hace usando el SDK de Amazon (https://sdk.amazonaws.com/js/aws-sdk-2.238.1.min.js).
La implementacion se encuentra en el formulario de registro de un usuario [`techu-enroll-form`](front/src/techu-enroll-form.html). El código de acceso a S3 se encuentra en [avatar_utils.js](front/js/avatar_utils.js)

La configuración de acceso al API se encuentra en la configuracion de la aplicación en el Store. [`js/config.json`](front/js/config.js)

```javascript
const _appconfig = {
    avatarStore: {
        bucketName : "techu-bank",
        bucketRegion : "XXXX",
        IdentityPoolId : "XXXXXX"
    }
}
```

###  **Localización**

Se utiliza el API de [ipdata.co](https://api.ipdata.co/) para obtener los datos de localización del usuario. En concreto, utilizamos el dato de la `ciudad`, que vamos a usar posteriormente para obtener las condiciones meteorológicas. Este API no requiere de ningún parámetro de entrada (la localización la hace en base a la direccion IP del usuario). Ejemplo de salida:

```json
{
    "ip": "89.107.183.3",
    "city": "Madrid",
    "region": "Madrid",
    ...
    "threat": {
        "is_tor": false,
        "is_proxy": false,
        "is_anonymous": false,
        "is_known_attacker": false,
        "is_known_abuser": false,
        "is_threat": false,
        "is_bogon": false
    }
```

###  **API de [Weather Conditions](http://openweathermap.org/weather-conditions) de [openweathermap.org](openweathermap.org)**. 

Tras el login del usuario en la aplicación, se obtiene la ciudad en la que se encuentra el usuario (punto anterior) y se hace una llamada a este API para obtener las condiciones meteorológicas (temperatura, viento e icono -soleado, lluvia, etc-). La llamada al API se hace pasando los siguientes parámetros:
    * q: Ciudad (ej: 'San Sebastian de los Reyes')
    * appid: Apikey generada para la aplicación.
    * init: 'metric'. Medidas en sistema métrico
    * lang: 'es'. Idioma de las descripciones (ej: 'lluvia moderada')

```
    http://api.openweathermap.org/data/2.5/weather?q=Alcobendas&appid=XXXXXXXX&units=metric&lang=es
```
---

## Spinner (velo)

En cada llamada al backend, para informar al usuario que debe esperar a la respuesta, se presenta una capa semitransparente con una animación. Para ellos se ha utilizado el componente del catálogo de cells [cells-molecule-spinner-veil](https://bbva-ether-cellscatalogs.appspot.com/?version=v200&view=docs#/component/cells-molecule-spinner-veil)

Para gestionar la activación y desactivación de esta capa de espera, desde el componente principal de la aplicación [techu-bank-app](./front/src/techu-bank-app.html) se escucha los eventos 'iron-ajax-request' y 'iron-ajax-response' (eventos lanzados por <iron-ajax>).

```javascript
  this.addEventListener('iron-ajax-request'...);
  this.addEventListener('iron-ajax-response'...);
```

Nota: Es necesario indicar en los componentes `<iron-ajax>` el atributo `bubbles` para que los eventos se propaguen.

## Scroll infinito

La operativa de movimientos de una cuenta incluye una lista de elementos (movimientos). Si el usuario hace scroll al final de la lista, se recarga con los siguientes 10 movimientos.

El componente visual de la lista [techu-movements-list.html](front/src/techu-movements-list.html) lanza un evento `more-movements-requested` cuando el scroll llega al final de la lista. 

```javascript
this.$.list.addEventListener('scroll', this.scrollHandlerFn.bind(this));
_onListScroll() {
  const list = this.$.list;
  if (list.scrollTop + list.clientHeight >= list.scrollHeight && this.hasMore) {
    this.dispatchEvent(new CustomEvent('more-movements-requested', {
      bubbles: true,
      composed: true
    }));
  }
 }
```
Este evento se captura en `techu-bank-app` que llama al DM para recuperar una página más. 

Cuando el DM detecta que no hay más movimientos, 
se actualiza en el Store el flag `moreMovements`. Para detectar si hay más movientos, en las consultas se solicitan siempre 11 elementos (aunque el DM sólo retorne 10 elementos). Cuando el flag `moreMovements` se estable a false, se elimina el listener que escucha el evento 'scroll' de la lista de movimientos. 

---
## Build

Se ha configurado el fichero [`polymer.json`](front/polymer.json) para definir la construcción de la aplicación en un hipotético entorno de producción. Se han añadido los fragments de los componentes que se cargan vía router, ya que si no, no genera correctamente las dependencias en bower_components.

```json
"fragments": [
    "src/techu-login.html",
    "src/techu-bank-app.html",
    "src/techu-dashboard.html",
    "src/techu-movements.html",
    "src/techu-transaction-form.html",
    "src/techu-not-found.html",
    "src/techu-enroll-form.html"
  ]
```

Se ha creado un script npm para lanzar la construcción. Como paso previo, se establece la configuración remota (backend en openshift)

```
npm run build
```

La construcción se lanza sin empaquetar en un solo fichero (bundle = false) y sin compilar los recursos Javascript

```json
"builds": [{
    "bundle": false,
    "js": {"minify": true},
    "css": {"minify": true},
    "html": {"minify": true}
  }]
```
---
## Docker

Se ha creado un fichero [Dockerfile](front/Dockerfile) para realizar el despliegue de la aplicación front en un servidor de estáticos.

La imagen Docker un servidor de estáticos basado en nodejs con el módulo [http-server](https://www.npmjs.com/package/http-server)

En la imagen se incluye el contenido completo de la carpeta `build`, incluyendo las carpetas de dependencias (`bower_components` y `node_modules`) que han sido construidas en el paso anterior.

```docker
#Imagen raiz
FROM node:10

RUN npm install -g http-server

#Copia de archivos
ADD build/default /techubank

EXPOSE 8080

CMD ["http-server", "/techubank"]
```

Para construir la imagen:

```
sudo docker build -t oarand/techubank_front .
```

Para ejecutar la imagen (en el puerto 8080, por ejemplo)

```
sudo docker run -p 8080:8080 oarand/techubank_front
```

La imagen se ha publicado en Docker Hub [oarand/techubank_front](https://hub.docker.com/r/oarand/techubank_front/)

---
## Openshift

La imagen oarand/techubank_front se ha desplegado en openshift.

La ruta de acceso a la imagen es:

[http://techubankfront-techu-bank.7e14.starter-us-west-2.openshiftapps.com/](http://techubankfront-techu-bank.7e14.starter-us-west-2.openshiftapps.com/)


---
## Créditos

* Imagenes. Pixabay
  * https://pixabay.com/es/puesta-de-sol-monte-arriba-paisaje-1757593/
  * https://pixabay.com/es/hong-kong-ciudad-urbana-rascacielos-1990268/
  * https://pixabay.com/es/berl%C3%ADn-gran-ciudad-metr%C3%B3polis-3157565/