const db = require('../db.js');

const ACCOUNTS_COLLECTION = "accounts";
const PAGE_SIZE = 5;
/**
 * Devuelve la lista de cuentas de un usuario
 * @param {Request} req Request express
 * @param {Response} res Response express
 */
const getAccounts = (req, res) => {
    console.log("GET /techu_bank/v1/accounts");

    /** extraemos el userid del token jwt */
    const user = req.login_info;
    console.log("user: " + JSON.stringify(user));

    const limit = parseInt(req.query.limit) || PAGE_SIZE;
    const skip = parseInt(req.query.skip) || 0;
    console.log('limit:' + limit);
    console.log('skip:' + skip);

    /** buscamos una cuentas que incluyan en el campo 'clients' el id del usuario logado */
    var filter = {
        "clients": user.id
    };
    db.get().collection(ACCOUNTS_COLLECTION).find(filter).skip(skip).limit(limit).toArray().then((accounts) => {
        console.log("accounts:" + JSON.stringify(accounts));

        /** validamos si el usuario tenia cuentas  */
        res.status(!accounts || accounts.length == 0 ? 204 : 200);
        res.send(accounts);
    }).catch(err => {
        console.error(err);
        res.status(err.status || 500);
        res.send(err.msg || err);
    });
}

module.exports = {
    getAccounts
};