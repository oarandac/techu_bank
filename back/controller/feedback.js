if (!process.env.GOOGLE_APPLICATION_CREDENTIALS) {
  process.env.GOOGLE_APPLICATION_CREDENTIALS =
    ".sec/gcp-service-account-file.json";
}
console.log("credentials path: " + process.env.GOOGLE_APPLICATION_CREDENTIALS);

// Imports the Google Cloud client library
const language = require("@google-cloud/language");

// Instantiates a client
const client = new language.LanguageServiceClient();

// mensajes de respuesta
const msg_1 =
  "Nos alegra escuchar que te ha gustado nuestra aplicación. Muchas gracias por tus palabras";
const msg_2 =
  "Seguiremos trabajando para que la experiencia de usuario sea mejor cada día";
const msg_3 =
  "Sentimos mucho que tu experiencia no haya sido satisfactoria. Seguiremos trabajando en mejorar la aplicación";

/**
 * Evalua el feedback enviado por el usuario y retorna un mensaje de respuesta en función del análisis realizado
 * @param  {} req
 * @param  {} res
 */
const evalFeedback = (req, res) => {
  console.log("POST /techu_bank/v1/feedback");

  /** extraemos el userid del token jwt */
  const user = req.login_info;
  console.log("user: " + JSON.stringify(user));

  const text = req.body.text || "";
  const lang = req.body.lang || "es";

  const document = {
    content: text,
    type: "PLAIN_TEXT",
    language: lang
  };

  client
    .analyzeSentiment({ document: document })
    .then(results => {
      const sentiment = results[0].documentSentiment;

      console.log(`Text: ${text}`);
      console.log(`Sentiment score: ${sentiment.score}`);
      console.log(`Sentiment magnitude: ${sentiment.magnitude}`);

      const response_text =
        sentiment.score < -0.2 ? msg_3 : sentiment.score > 0.2 ? msg_1 : msg_2;

      console.log(`Mensaje de respuesta: ${response_text}`);

      res.send({
        response_text,
        score: sentiment.score
      });
    })
    .catch(err => {
      console.error("ERROR:", err);
      res.status(500);
      res.send({
        msg: "Se ha producido un error"
      });
    });
};

module.exports = {
  evalFeedback
};
