const db = require("../db.js");
const uid = require("uid");

const PAGE_SIZE = 5;

/**
 * Devuelve la lista de cuentas de un usuario
 * @param {Request} req Request express
 * @param {Response} res Response express
 */
const getTransactions = (req, res) => {
  console.log("GET /techu_bank/v1/accounts/:id/transactions");

  var accId = parseInt(req.params.id) || 0;

  /** extraemos el userid del token jwt */
  const user = req.login_info;
  console.log("user: " + JSON.stringify(user));

  const limit = parseInt(req.query.limit) || PAGE_SIZE;
  const skip = parseInt(req.query.skip) || 0;
  console.log("limit:" + limit);
  console.log("skip:" + skip);

  /** buscamos una cuenta con el id indicado Y que pertenezca al usuario logado */
  var filter = {
    id: accId,
    clients: user.id
  };
  db
    .get()
    .collection(db.ACCOUNTS_COLLECTION)
    .findOne(filter)
    .then(account => {
      console.log("account:" + JSON.stringify(account));
      if (!account) {
        return Promise.reject({ status: 404, msg: "No account found" });
      }
      /** filtro. transacciones con el id de cuenta indicado */
      var txs_filter = {
        account_id: accId
      };
      console.log(JSON.stringify(txs_filter));
      return db
        .get()
        .collection(db.TRANSACTIONS_COLLECTION)
        .find(txs_filter)
        .skip(skip)
        .limit(limit)
        .sort({ date: -1 })
        .toArray();
    })
    .then(transactions => {
      console.log("transactions:" + JSON.stringify(transactions));
      if (transactions.length == 0) {
        /** no hay movimientos */
        res.status(204);
        res.send();
      } else {
        /** devolvemos las transacciones */
        res.send(transactions);
      }
    })
    .catch(err => {
      console.error(err);
      res.status(err.status || 500);
      res.send(err.msg || err);
    });
};

const addTransaction = (req, res) => {
  console.log("POST /techu_bank/v1/accounts/:id/transactions");

  const transaction = {
    account_id: parseInt(req.params.id) || 0,
    amount: parseFloat(req.body.amount) || 0,
    description: req.body.description || "",
    transaction_id: uid(10),
    date: new Date().toJSON()
  };

  // en función del tipo, cambiamos signo de la cantidad
  let type = req.body.type;
  if (type !== "deposit") {
    transaction.amount *= -1;
  }

  console.log("transaction: " + JSON.stringify(transaction));

  /** extraemos el userid del token jwt */
  const user = req.login_info;
  console.log("user: " + JSON.stringify(user));

  /** buscamos una cuenta con el id indicado Y que pertenezca al usuario logado */
  var filter = {
    id: transaction.account_id,
    clients: user.id
  };
  console.log("filter: " + JSON.stringify(filter));
  db
    .get()
    .collection(db.ACCOUNTS_COLLECTION)
    .findOne(filter)
    .then(account => {
      // insertamos movimiento
      console.log("account:" + JSON.stringify(account));
      if (!account) {
        return Promise.reject({ status: 404, msg: "No account found" });
      }
      return db
        .get()
        .collection(db.TRANSACTIONS_COLLECTION)
        .insert(transaction);
    })
    .then(() => {
      console.log("Transaction added OK");
      // actualizamos saldo
      return db
        .get()
        .collection(db.ACCOUNTS_COLLECTION)
        .update(filter, { $inc: { balance: transaction.amount } });
    })
    .then(() => {
      console.log("Balance updated OK");
      // si es una transferencia, actualizamos saldo en la cuenta destino
      if (type === "transfer") {
        const destFilter = {
          id: req.body.destination
        };
        console.log("Transfer destination: "+destFilter.id);
        return db
          .get()
          .collection(db.ACCOUNTS_COLLECTION)
          .update(destFilter, { $inc: { balance: transaction.amount * -1 } });
      } else {
        return Promise.resolve();
      }
    })
    .then(() => {
      if (type === "transfer") {

        const transaction_dest = {
          account_id: req.body.destination,
          amount: transaction.amount * -1,
          description: transaction.description,
          transaction_id: uid(10),
          date: new Date().toJSON()
        };
        return db
          .get()
          .collection(db.TRANSACTIONS_COLLECTION)
          .insert(transaction_dest);
      } else {
        return Promise.resolve();
      }
    })
    .then(() => {
      console.log("Transaction completed OK");
      res.status(201);
      res.send(transaction);
    })
    .catch(err => {
      console.error(err);
      res.status(err.status || 500);
      res.send(err.msg || err);
    });
};

module.exports = {
  getTransactions,
  addTransaction
};
