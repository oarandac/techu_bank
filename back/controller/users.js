const tokenizer = require("../token.js");
const bcrypt = require("bcrypt");
const db = require("../db.js");

/**
 * Login
 * @param {Request} req Request express
 * @param {Response} res Response express
 */
const doLogin = (req, res) => {
  console.log("POST /techu_bank/v1/login");

  /** read params */
  const userToFind = {
    email: req.body.email || ""
  };
  var password = req.body.password || "";

  db
    .get()
    .collection(db.USERS_COLLECTION)
    .findOne(userToFind)
    .then(foundUser => {
      console.log("foundUser:" + JSON.stringify(foundUser));
      if (!foundUser) {
        console.log("User not found in DB");
        /** devolvemos 401 para no dar pistas si el usuario existe o no */
        res.status(401);
        res.send({
          msg: "Unauthorized access"
        });
      } else {
        /** validamos la contraseña usando bcrypt. En Mongo esta el hash de la passw  */
        var hash = foundUser.password;
        bcrypt.compare(password, hash).then(result => {
          if (result) {
            console.log("password OK");
            /** generamos el token */
            var token = tokenizer.generate(foundUser);

            /** devolvemos el token como una cabecera de la respuesta */
            res.set("token", token);

            /** devolvemos el usuario encontrado, aunque por seguridad eliminamos la propiedad 'password' */
            delete foundUser.password;
            res.send({
              user: foundUser
            });
          } else {
            console.log("password KO");
            res.status(401);
            res.send({
              msg: "Unauthorized access"
            });
          }
        });
      }
    })
    .catch(err => {
      console.error(err);
      res.status(err.status || 500);
      res.send(err.msg || err);
    });
};

const doLogout = (req, res) => {
  console.log("POST /techu_bank/v1/logout");

  /** invalidamos token */
  res.set("token", "");

  res.send({
    msg: "logout ok"
  });
};

const doCreateUser = (req, res) => {
  console.log("POST /techu_bank/v1/users");

  /** read params */
  const newUser = {
    email: req.body.email || "",
    first_name: req.body.first_name || "",
    last_name: req.body.last_name || "",
    country: req.body.country
  };
  if (req.body.avatar) {
    newUser.avatar = req.body.avatar;
  }

  /** hash the password */
  bcrypt
    .hash(req.body.password, 10)
    .then(hash => {
      console.log("hash:" + hash);
      newUser.password = hash;

      db
        .get()
        .collection(db.USERS_COLLECTION)
        .find({})
        .sort({ id: -1 })
        .limit(1)
        .toArray()
        .then(result => {
          /** insertamos el nuevo usuario */
          console.log("last user id: " + result[0].id);
          newUser.id = result[0].id + 1;
          console.log("newUser: " + JSON.stringify(newUser));
          return db
            .get()
            .collection(db.USERS_COLLECTION)
            .insert(newUser);
        })
        .then(result => {
          console.log("User inserted OK");
          // creamos cuenta corriente nueva
          // obtenemos el idAccount más alto
          return db
            .get()
            .collection(db.ACCOUNTS_COLLECTION)
            .find({})
            .sort({ id: -1 })
            .limit(1)
            .toArray()
        })
        .then(result => {
          const nextAccId = result[0].id + 1;
          const newAccount = {
            id: nextAccId,
            iban: generateRandomIBAN(),
            type: "saving",
            balance: 0,
            currency: "EUR",
            alias: newUser.first_name + " account",
            clients: [newUser.id]
          };
          return db
          .get()
          .collection(db.ACCOUNTS_COLLECTION)
          .insert(newAccount);
        })
        .then(result => {
          console.log("User & Account created OK")
          res.status(201);
          res.send();
        })
        .catch(err => {
          console.error(err);
          res.status(err.status || 500);
          res.send({ msg: err.message || err });
        });
    })
    .catch(err => {
      console.error("Error al obtener hash de la contraseña: " + err);
      res.status(500);
      res.send({ msg: err.msg || err });
    });
};

const generateRandomIBAN = () => {
    return "ES018200976768768768";
}

module.exports = {
  doLogin,
  doLogout,
  doCreateUser
};
