/**
 * Modulo para generar y validar tokens jwt
 * @module token 
 * */
const jwt = require('jsonwebtoken');

const config = require('config');

/**
 * Genera un token JWT a partir de un objeto user.
 * @param {object} user - objeto con campos email, id.
 * @return {string} jwt token.
 */
const generate = (user) => {
    var payload = {
        "email": user.email,
        "id": user.id,
        "time": new Date()
    };
    var token = jwt.sign(payload, config.get('jwt.secret'), {
        expiresIn: config.get('jwt.tokenExpireTime')
    });
    return token;
}

/**
 * Express middleware que valida un token JWT 
 * @param {request} req - express request.
 * @param {response} res - express response.
 * @param {next} next - siguiente función a ejecutar si todo va OK
 * @return {string} // TODO
 */
const validate = (req, res, next) => {
    var token = req.headers.token;
    console.log("token: " + token);
    jwt.verify(token, config.get('jwt.secret'), (err, decoded) => {
        console.log("token decoded: " + JSON.stringify(decoded));
        if (err) {
            console.log("err: " + JSON.stringify(err));
            return res.status(401).send({ auth: false, message: 'Invalid token.' });
        }
        req.login_info = {
            login: decoded.email,
            id: decoded.id
        };

        console.log("token ok");

        next();
    });
};

/** exportamos las dos funciones */
module.exports = {
    generate,
    validate
}
