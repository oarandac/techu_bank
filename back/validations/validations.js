const Joi = require('joi');

const login = {
  body: {
    email: Joi.string().email().required(),
    password: Joi.string().regex(/[a-zA-Z0-9]{6,30}/).required()
  }
};

const get_accounts = {
  headers: {
    token: Joi.string().required(),
  },
  query: {
    limit: Joi.number().integer().positive().optional(),
    skip: Joi.number().integer().min(0).optional()
  }
};

const get_transactions = {
  headers: {
    token: Joi.string().required(),
  },
  params: {
    id: Joi.number().integer().positive().required()
  },
  query: {
    limit: Joi.number().integer().positive().optional(),
    skip: Joi.number().integer().min(0).optional()
  }
};
const post_transaction = {
  headers: {
    token: Joi.string().required(),
  },
  params: {
    id: Joi.number().integer().positive().required()
  },
  body: {
    description: Joi.string().regex(/[a-zA-Z ]/).required(),
    amount: Joi.number().precision(2).required(),
    type: Joi.string().valid('deposit','payment','transfer').required(), 
    destination: Joi.number().integer().min(1).optional()
  }
};
//destination: Joi.string().optional().regex(/[A-Z]{2}\d{2} ?\d{4} ?\d{4} ?\d{4} ?\d{4} ?[\d]{0,2}/)
const post_user = {
  body: {
    first_name: Joi.string().regex(/[a-zA-Z ]/).required(),
    last_name: Joi.string().regex(/[a-zA-Z ]/).required(),
    email: Joi.string().email().required(),
    country: Joi.string().length(2).uppercase().required(),
    password: Joi.string().regex(/[a-zA-Z0-9]{6,30}/).required(),
    avatar: Joi.string().uri({allowRelative:false}).optional()
  }
};

const post_feedback = {
  body: {
    text: Joi.string().required()
  }
};

module.exports = {
  login,
  get_accounts,
  get_transactions,
  post_transaction,
  post_user, 
  post_feedback
}