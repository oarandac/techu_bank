//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const token = require('../token.js');
const app = require('../server.js');
const expect = chai.expect;
const mongo = require('mongodb').MongoClient;
const config = require('config');

chai.use(chaiHttp);

var client, database;

/** test data, collección de usuarios */
const users = require('../test-data/techu_bank.users.json');
const accounts = require('../test-data/techu_bank.accounts.json');

const login_user1 = {
    email: users[0].email,
    password: "techumola"
}

const login_user2 = {
    email: users[1].email,
    password: "techumola"
}

describe('Testing Techu Bank - USERS', function () {

    before(function () {
        /** preparamos la BBDD con los datos del test, coleccion: Users */
        console.log("Before.....");

        return mongo.connect(config.get('mlab.url')).then(_client => {
            client = _client;
            database = client.db(config.get('mlab.dbname'));
            return database.collection('users').insert(users);
        }).then(() => {
            console.log("Insert Users OK");
            return database.collection('accounts').insert(accounts);
        }).then(() => {
            console.log("Insert Accounts OK");
        }).catch(err => {
            console.error("Insert KO");
        });


    });

    describe('/POST login', () => {
        it('debe retornar token y datos del usuario logado', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user2)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;
                    expect(res.body).to.have.property("user");
                    expect(res.body.user).to.have.property("id", 2);
                    expect(res.body.user).to.have.property("first_name", "Herc");
                    expect(res.body.user).to.have.property("last_name", "Haggish");

                    done();
                });
        });
        it('debe retornar error al logarse con un usuario no existente, no debe incluir token', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send({ 'email': 'XXXX@YYY.com', 'password': 'password' })
                .end((err, res) => {
                    res.should.have.status(401);
                    expect(res).not.to.have.header('token');
                    expect(res).to.be.json;

                    done();
                });
        });
        it('debe retornar error cuando la contraseña es incorrecta, no debe incluir token', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send({ 'email': login_user2.email, 'password': 'XXXXXXX' })
                .end((err, res) => {
                    res.should.have.status(401);
                    expect(res).not.to.have.header('token');
                    expect(res).to.be.json;

                    done();
                });
        });
        it('debe retornar error cuando faltan parámetros obligatorios, no debe incluir token', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send({ 'email': login_user2.email })
                .end((err, res) => {
                    res.should.have.status(400);
                    expect(res).not.to.have.header('token');
                    expect(res).to.be.json;

                    done();
                });
        });
        it('debe retornar error el email no tiene el formato correcto, no debe incluir token', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send({ 'email': 'aa6677.com' })
                .end((err, res) => {
                    res.should.have.status(400);
                    expect(res).not.to.have.header('token');
                    expect(res).to.be.json;

                    done();
                });
        });
    });

    describe('/POST users', () => {
        let newUser = {
            "first_name": "test",
            "last_name": "user",
            "email": "user@techu.com",
            "country": "IT",
            "password": "password"
        }
        it('debe crear un nuevo usuario', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/users')
                .set('Content-Type', 'application/json')
                .send(newUser)
                .end((err, res) => {
                    res.should.have.status(201);
                    expect(res.body).to.be.empty;

                    done();
                });
        });

        it('debe retornar error al intentar registrar un nuevo usuario con email ya existente', (done) => {
            newUser.email = login_user1.email;
            chai.request(app)
                .post('/techu_bank/v1/users')
                .set('Content-Type', 'application/json')
                .send(newUser)
                .end((err, res) => {
                    res.should.have.status(500);
                    console.log("res.body=" + JSON.stringify(res.body) + ".");
                    expect(res.body).to.have.property("msg");
                    expect(res.body.msg).to.have.string("duplicate key error");

                    done();
                });
        });
        let newUser2 = {
            "last_name": "user",
            "email": "user@techu.com",
            "country": "IT",
            "password": "password"
        }
        it('debe retornar error al faltar el parámetro obligatorio "first_name"', (done) => {
            newUser.email = login_user1.email;
            chai.request(app)
                .post('/techu_bank/v1/users')
                .set('Content-Type', 'application/json')
                .send(newUser2)
                .end((err, res) => {
                    res.should.have.status(400);
                    expect(res).not.to.have.header('token');
                    expect(res).to.be.json;

                    done();
                });
        });
    });

    after(function () {
        /** vaciamos las colecciones */
        console.log("After.....");

        return database.collection('users').remove({})
            .then(() => {
                console.log("Remove Users OK");
                return database.collection('accounts').remove({})
            }).then(() => {
                console.log("Remove Accounts OK");
                client.close(true);
            }).catch(err => {
                console.error("Remove KO");
                console.error(err);
                client.close(true);
            });
    });
});