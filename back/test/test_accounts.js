//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const token = require('../token.js');
const app = require('../server.js');
const expect = chai.expect;
const mongo = require('mongodb').MongoClient;
const config = require('config');

chai.use(chaiHttp);

var client, database;

/** test data Colleciones necesarias para estos test: users y accounts */
const users = require('../test-data/techu_bank.users.json');
const accounts = require('../test-data/techu_bank.accounts.json');

/** usuarios para las pruebas */
const login_user1 = {
    email: users[0].email,
    password: "techumola"
}

const login_user2 = {
    email: users[1].email,
    password: "techumola"
}

describe('Testing Techu Bank - ACCOUNTS', function () {

    before(function () {
        /** insertamos datos de pruebas en la BBDD. Colescciones users y accounts */
        console.log("Before.....");

        return mongo.connect(config.get('mlab.url')).then(_client => {
            client = _client;
            database = client.db(config.get('mlab.dbname'));
            return database.collection('users').insert(users);
        }).then(() => {
            console.log("Insert Users OK");
            return database.collection('accounts').insert(accounts);
        }).then(() => {
            console.log("Insert Accounts OK");
        }).catch(err => {
            console.error("Insert KO");
        });

    });

    /** 
     * Test /GET accounts
     * 
    */
    describe('/GET accounts', () => {
        it('debe retornar todas las cuentas de un usuario', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user2)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;

                    const user1_token = res.header.token;
                    chai.request(app)
                        .get('/techu_bank/v1/accounts')
                        .set('token', user1_token)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('array');
                            res.body.length.should.be.eql(5);
                            done();
                        });
                });
        });

        it('debe retornar que no hay cuentas', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user1)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;

                    const user2_token = res.header.token;

                    chai.request(app)
                        .get('/techu_bank/v1/accounts')
                        .set('token', user2_token)
                        .end((err, res) => {
                            res.should.have.status(204);
                            done();
                        });
                });
        });
        it('debe retornar error 401, por token inválido', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user1)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;

                    chai.request(app)
                        .get('/techu_bank/v1/accounts')
                        .set('token', 'XXXXXXXXX')
                        .end((err, res) => {
                            res.should.have.status(401);
                            done();
                        });
                });

        });
    });

    /** 
     * Test /GET accounts
     * 
    */
    describe('/GET accounts con paginación', () => {
        it('debe retornar el número de cuentas marca el param limit', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user2)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;
                    const user2_token = res.header.token;

                    chai.request(app)
                        .get('/techu_bank/v1/accounts')
                        .set('token', user2_token)
                        .query({ limit: 4 })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('array');
                            res.body.length.should.be.eql(4);
                            done();
                        });
                });

        });

        it('debe retornar el número de cuentas inferior al param limit ya que hay menos cuentas', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user2)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;
                    const user2_token = res.header.token;

                    chai.request(app)
                        .get('/techu_bank/v1/accounts')
                        .set('token', user2_token)
                        .query({ limit: 10 })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('array');
                            res.body.length.should.be.eql(6);
                            done();
                        });
                });

        });

        it('skip. evitamos las dos primeras cuentas', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user2)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;
                    const user2_token = res.header.token;

                    chai.request(app)
                        .get('/techu_bank/v1/accounts')
                        .set('token', user2_token)
                        .query({ limit: 5, skip: 2 })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('array');
                            res.body.length.should.be.eql(4);
                            expect(res.body[0]).to.have.property("id", 3);
                            done();
                        });
                });
        });

        it('error al indicar un tamaño de pagina inválido', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user2)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;
                    const user2_token = res.header.token;

                    chai.request(app)
                        .get('/techu_bank/v1/accounts')
                        .set('token', user2_token)
                        .query({ limit: 'aa', skip: -1 })
                        .end((err, res) => {
                            res.should.have.status(400);
                            expect(res).to.be.json;
                            done();
                        });
                });
        });
    });


    after(function () {
        /** vaciamos las dos colecciones usadas */
        console.log("After.....");

        return database.collection('users').remove({})
            .then(() => {
                console.log("Remove Users OK");
                return database.collection('accounts').remove({})
            }).then(() => {
                console.log("Remove Accounts OK");
                client.close(true);
            }).catch(err => {
                console.error("Remove KO");
                console.error(err);
                client.close(true);
            });
    });
});