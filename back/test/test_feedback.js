//During the test the env variable is set to test
process.env.NODE_ENV = "test";

const chai = require("chai");
const chaiHttp = require("chai-http");
const should = chai.should();
const token = require("../token.js");
const app = require("../server.js");
const expect = chai.expect;
const mongo = require("mongodb").MongoClient;
const config = require("config");

chai.use(chaiHttp);

var client, database;

/** test data, collección de usuarios */
const users = require("../test-data/techu_bank.users.json");

const login_user1 = {
  email: users[0].email,
  password: "techumola"
};

describe("Testing Techu Bank - FEEDBACK", function() {
  before(function() {
    /** preparamos la BBDD con los datos del test, coleccion: Users */
    console.log("Before.....");

    return mongo
      .connect(config.get("mlab.url"))
      .then(_client => {
        client = _client;
        database = client.db(config.get("mlab.dbname"));
        return database.collection("users").insert(users);
      })
      .then(() => {
        console.log("Insert Users OK");
      })
      .catch(err => {
        console.error("Insert KO");
      });
  });

  describe("/POST feedback", () => {
    it("debe retornar mensaje de agradecimiento por feedbak positivo", done => {
      chai
        .request(app)
        .post("/techu_bank/v1/login")
        .set("Content-Type", "application/json")
        .send(login_user1)
        .end((err, res) => {
          res.should.have.status(200);
          expect(res).to.have.header("token");
          expect(res).to.be.json;

          const user1_token = res.header.token;
          chai
            .request(app)
            .post("/techu_bank/v1/feedback")
            .set("token", user1_token)
            .send({ text: "Esta genial. Muy buen trabajo" })
            .end((err, res) => {
              res.should.have.status(200);
              expect(res).to.be.json;
              expect(res.body).to.have.property(
                "response_text",
                "Nos alegra escuchar que te ha gustado nuestra aplicación. Muchas gracias por tus palabras"
              );
              expect(res.body).to.have.property("score");

              done();
            });
        });
    });
    it("debe retornar mensaje de agradecimiento por feedbak intermedio", done => {
      chai
        .request(app)
        .post("/techu_bank/v1/login")
        .set("Content-Type", "application/json")
        .send(login_user1)
        .end((err, res) => {
          res.should.have.status(200);
          expect(res).to.have.header("token");
          expect(res).to.be.json;

          const user1_token = res.header.token;
          chai
            .request(app)
            .post("/techu_bank/v1/feedback")
            .set("token", user1_token)
            .send({ text: "No esta mal. Podría mejorar" })
            .end((err, res) => {
              res.should.have.status(200);
              expect(res).to.be.json;
              expect(res.body).to.have.property(
                "response_text",
                "Seguiremos trabajando para que la experiencia de usuario sea mejor cada día"
              );
              expect(res.body).to.have.property("score");

              done();
            });
        });
    });
    it("debe retornar mensaje de agradecimiento por feedbak negativo", done => {
      chai
        .request(app)
        .post("/techu_bank/v1/login")
        .set("Content-Type", "application/json")
        .send(login_user1)
        .end((err, res) => {
          res.should.have.status(200);
          expect(res).to.have.header("token");
          expect(res).to.be.json;

          const user1_token = res.header.token;
          chai
            .request(app)
            .post("/techu_bank/v1/feedback")
            .set("token", user1_token)
            .send({ text: "Bastante regular, no me ha gustado demasiado" })
            .end((err, res) => {
              res.should.have.status(200);
              expect(res).to.be.json;
              expect(res.body).to.have.property(
                "response_text",
                "Sentimos mucho que tu experiencia no haya sido satisfactoria. Seguiremos trabajando en mejorar la aplicación"
              );
              expect(res.body).to.have.property("score");

              done();
            });
        });
    });
    it("debe retornar mensaje de error al faltar el campo text", done => {
      chai
        .request(app)
        .post("/techu_bank/v1/login")
        .set("Content-Type", "application/json")
        .send(login_user1)
        .end((err, res) => {
          res.should.have.status(200);
          expect(res).to.have.header("token");
          expect(res).to.be.json;

          const user1_token = res.header.token;
          chai
            .request(app)
            .post("/techu_bank/v1/feedback")
            .set("token", user1_token)
            .send({})
            .end((err, res) => {
              res.should.have.status(400);
              expect(res).to.be.json;

              done();
            });
        });
    });

    it("debe retornar mensaje de error al faltar token", done => {
      chai
        .request(app)
        .post("/techu_bank/v1/feedback")
        .send({})
        .end((err, res) => {
          res.should.have.status(400);
          expect(res).to.be.json;

          done();
        });
    });
  });

  after(function() {
    /** vaciamos las colecciones */
    console.log("After.....");

    return database
      .collection("users")
      .remove({})
      .then(() => {
        console.log("Remove Users OK");
      })
      .catch(err => {
        console.error("Remove KO");
        console.error(err);
        client.close(true);
      });
  });
});
