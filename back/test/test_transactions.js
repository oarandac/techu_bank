//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const token = require('../token.js');
const app = require('../server.js');
const expect = chai.expect;
const mongo = require('mongodb').MongoClient;
const config = require('config');

chai.use(chaiHttp);

var client, database;

/** test data */
const users = require('../test-data/techu_bank.users.json');
const accounts = require('../test-data/techu_bank.accounts.json');
const transactions = require('../test-data/techu_bank.transactions.json');

const login_user1 = {
    email: users[0].email,
    password: "techumola"
}

const login_user2 = {
    email: users[1].email,
    password: "techumola"
}

describe('Testing Techu Bank - TRANSACTIONS', function () {

    before(function () {
        // runs before all tests in this block
        console.log("Before.....");

        return mongo.connect(config.get('mlab.url')).then(_client => {
            client = _client;
            database = client.db(config.get('mlab.dbname'));
            return database.collection('users').insert(users);
        }).then(() => {
            console.log("Insert Users OK");
            return database.collection('accounts').insert(accounts);
        }).then(() => {
            console.log("Insert Accounts OK");
            return database.collection('transactions').insert(transactions);
        }).then(() => {
            console.log("Insert Transactions OK");
        }).catch(err => {
            console.error("Insert KO");
        });

    });


    /** 
     * Test /GET accounts
     * 
    */
    describe('/GET transactions', () => {
        it('debe retornar todas los movimientos de una cuenta', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user2)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;

                    const user2_token = res.header.token;
                    chai.request(app)
                        .get('/techu_bank/v1/accounts/1/transactions')
                        .set('token', user2_token)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('array');
                            res.body.length.should.be.eql(5);
                            expect(res.body[0]).to.have.property("transaction_id", "8m49qkdb4k");
                            done();
                        });
                });
        });

        it('debe retornar el sexto elemento de una coleccion', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user2)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;

                    const user2_token = res.header.token;
                    chai.request(app)
                        .get('/techu_bank/v1/accounts/1/transactions')
                        .set('token', user2_token)
                        .query({ skip: 5 })
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('array');
                            res.body.length.should.be.eql(1);
                            expect(res.body[0]).to.have.property("transaction_id", "a767y89i34");
                            done();
                        });
                });
        });

        it('debe retornar error, el usuario no es el propietario de la cuenta', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user1)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;

                    const user1_token = res.header.token;
                    chai.request(app)
                        .get('/techu_bank/v1/accounts/1/transactions')
                        .set('token', user1_token)
                        .query({ skip: 5 })
                        .end((err, res) => {
                            res.should.have.status(404);
                            done();
                        });
                });
        });

        it('debe retornar error, la cuenta no existe', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user2)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;

                    const user2_token = res.header.token;
                    chai.request(app)
                        .get('/techu_bank/v1/accounts/1000/transactions')
                        .set('token', user2_token)
                        .query({ skip: 5 })
                        .end((err, res) => {
                            res.should.have.status(404);
                            done();
                        });
                });
        });

        it('debe retornar error, la cuenta no tiene el formato correcto', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user2)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;

                    const user2_token = res.header.token;
                    chai.request(app)
                        .get('/techu_bank/v1/accounts/xasd/transactions')
                        .set('token', user2_token)
                        .query({ skip: 5 })
                        .end((err, res) => {
                            res.should.have.status(400);
                            expect(res).to.be.json;
                            done();
                        });
                });
        });

        it('debe retornar error al haber un error en el formato de la paginación', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user2)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;

                    const user2_token = res.header.token;
                    chai.request(app)
                        .get('/techu_bank/v1/accounts/1/transactions')
                        .set('token', user2_token)
                        .query({ skip: -1 })
                        .end((err, res) => {
                            res.should.have.status(400);
                            expect(res).to.be.json;
                            done();
                        });
                });
        });
    });

    /** 
     * Test /GET accounts
     * 
    */
    describe('/POST transactions', () => {
        it('debe añadir un nuevo movimiento a la cuenta', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user2)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;

                    const user2_token = res.header.token;
                    const transaction = {
                        description: "test description",
                        amount: 200.45,
                        type: 'payment'
                    }

                    chai.request(app)
                        .post('/techu_bank/v1/accounts/1/transactions')
                        .set('token', user2_token)
                        .set('Content-Type', 'application/json')
                        .send(transaction)
                        .end((err, res) => {
                            res.should.have.status(201);
                            res.body.should.be.a('object');
                            expect(res.body).to.have.property("description", transaction.description);
                            expect(res.body).to.have.property("amount", transaction.amount * -1);
                            expect(res.body).to.have.property("account_id", 1);
                            expect(res.body).to.have.property("transaction_id");
                            expect(res.body).to.have.property("date");
                            done();
                        });
                });
        });

        it('debe retornar error ya que el usuario logado no es propietario de la cuenta', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user1)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;

                    const user1_token = res.header.token;
                    const transaction = {
                        description: "test description",
                        amount: -200.45,
                        type: 'payment'
                    }

                    chai.request(app)
                        .post('/techu_bank/v1/accounts/1/transactions')
                        .set('token', user1_token)
                        .set('Content-Type', 'application/json')
                        .send(transaction)
                        .end((err, res) => {
                            res.should.have.status(404);
                            res.body.should.be.a('object');
                            done();
                        });
                });
        });

        it('debe retornar error ya que la cuenta no existe', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user1)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;

                    const user1_token = res.header.token;
                    const transaction = {
                        description: "test description",
                        amount: -200.45,
                        type: 'payment'
                    }

                    chai.request(app)
                        .post('/techu_bank/v1/accounts/1000/transactions')
                        .set('token', user1_token)
                        .set('Content-Type', 'application/json')
                        .send(transaction)
                        .end((err, res) => {
                            res.should.have.status(404);
                            res.body.should.be.a('object');
                            done();
                        });
                });
        });

        it('debe retornar error al crear un movimiento por error en formato de params de entrada', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user2)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;

                    const user2_token = res.header.token;
                    const transaction = {
                        amount: 'abc'
                    }

                    chai.request(app)
                        .post('/techu_bank/v1/accounts/1/transactions')
                        .set('token', user2_token)
                        .set('Content-Type', 'application/json')
                        .send(transaction)
                        .end((err, res) => {
                            res.should.have.status(400);
                            expect(res).to.be.json;

                            done();
                        });
                });
        });

        it('debe retornar error enviar un formato de cuenta incorrecto', (done) => {
            chai.request(app)
                .post('/techu_bank/v1/login')
                .set('Content-Type', 'application/json')
                .send(login_user2)
                .end((err, res) => {
                    res.should.have.status(200);
                    expect(res).to.have.header('token');
                    expect(res).to.be.json;

                    const user2_token = res.header.token;
                    const transaction = {
                        amount: 'abc'
                    }

                    chai.request(app)
                        .post('/techu_bank/v1/accounts/ACC/transactions')
                        .set('token', user2_token)
                        .set('Content-Type', 'application/json')
                        .send(transaction)
                        .end((err, res) => {
                            res.should.have.status(400);
                            expect(res).to.be.json;

                            done();
                        });
                });
        });
    });


    after(function () {
        // runs before all tests in this block
        console.log("After.....");

        return database.collection('users').remove({})
            .then(() => {
                console.log("Remove Users OK");
                return database.collection('accounts').remove({})
            }).then(() => {
                console.log("Remove Accounts OK");
                return database.collection('transactions').remove({})
            }).then(() => {
                console.log("Remove Transactions OK");
                client.close(true);
            }).catch(err => {
                console.error("Remove KO");
                console.error(err);
                client.close(true);
            });
    });
});