const bcrypt = require('bcrypt');

/**
 * 
 * @param {String} password Contraseña
 * @returns {Promid}
 */
const hashPasswd = (password) => {
    return bcrypt.hash(password, 10).then((h)=> h);
}

/**
 * Valida una contraseña con el valor Hash
 * @param {String} password contraseña a validar
 * @param {String} hash valor hash
 * @returns {Promise} promesa con el resultado (true|false) de la validación
 */
const validatePasswd = (password, hash) => {
    return bcrypt.compare(password, hash);
}

let hash = hashPasswd('techumola');
hash.then((h)=> {
    console.log('hash: ' + h);
    console.log("OK");    
});


// "techumola" : "$2a$10$sndkyiUz6H2ZHkH66PJKX.2ZbD1RcDztgKrXTfPycsLHnWBsQxPdS"



function newFunction() {
    validatePasswd('myPassword2', '$2a$10$umhm8U/LKDjGYDIgpREd9uMhI9.HovZ1jIRv0nl9KpGLP.ieOa4tK');
    validatePasswd('myPassword', '$2a$10$umhm8U/LKDjGYDIgpREd9uMhI9.HovZ1jIRv0nl9KpGLP.ieOa4tK');
    validatePasswd('myPassword', '$2a$10$0ubY4QlNE8VssD7V5S63DOadvxZt8a6vli7t0SnDvbYhQl2lNp7KG');
}

