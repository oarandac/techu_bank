var mongo = require('mongodb').MongoClient

const USERS_COLLECTION = "users";
const TRANSACTIONS_COLLECTION = "transactions";
const ACCOUNTS_COLLECTION = "accounts";

/** conexion con la bbdd */
const state = {
  db: null,
}

const connect = function(url, dbname, done) {
  if (state.db) return done()

  mongo.connect(url, function(err, db) {
    if (err) return done(err)
    state.db = db.db(dbname);
    done()
  })
}

/** metodo para obetenr la conexion a la bbdd */
const get = function() {
  return state.db
}

const close = function(done) {
  if (state.db) {
    state.db.close(function(err, result) {
      state.db = null
      done(result)
    })
  }
}

module.exports = {
    connect, 
    get,
    close, 
    USERS_COLLECTION, 
    ACCOUNTS_COLLECTION, 
    TRANSACTIONS_COLLECTION
}