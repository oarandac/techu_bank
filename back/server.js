/** express */
const express = require('express');
const bodyParser = require('body-parser');
const config = require('config');
const cors = require('cors');
const validate = require('express-validation');
const token = require('./token.js');
const db = require('./db.js');
const controller_users = require('./controller/users.js');
const controller_accounts = require('./controller/accounts.js');
const controller_transactions = require('./controller/transactions.js');
const controller_feedback = require('./controller/feedback.js');

const validations = require('./validations/validations.js');


/** instanciamos la app express */
const app = express();
/** seleccionamos el perser json */
app.use(bodyParser.json());

/** habilitamos cors */
var corsOptions = {
    'allowedHeaders': ['token', 'Content-Type'],
    'exposedHeaders': ['token'],
    'origin': '*',
    'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
    'preflightContinue': false
};
app.use(cors(corsOptions));

/** puerto donde escucha el servidor */
var port = config.get('server.port') || 3000;

// Connect to Mongo on start
db.connect(config.get('mlab.url'), config.get('mlab.dbname'), function (err) {
    if (err) {
        console.log('No hay conexion a Mongo DB.');
        console.log(err);
        process.exit(1)
    } else {
        /** arrancamos servidor en el puerto seleccionado */
        var server = app.listen(port);
        console.log("API listening on port " + port);

        /**  endpoint de bienvenida */
        app.get('/techu_bank/v1', (req, res) => {
            console.log("GET /techu_bank/v1");
            res.send({
                "msg": "Techu_bank API"
            });
        });

        /** endpoint de login */
        app.post('/techu_bank/v1/login', validate(validations.login), controller_users.doLogin);

        app.get('/techu_bank/v1/accounts', validate(validations.get_accounts), token.validate, controller_accounts.getAccounts);

        app.get('/techu_bank/v1/accounts/:id/transactions', validate(validations.get_transactions), token.validate, controller_transactions.getTransactions);

        app.post('/techu_bank/v1/accounts/:id/transactions', validate(validations.post_transaction), token.validate, controller_transactions.addTransaction);

        app.post('/techu_bank/v1/users', validate(validations.post_user), controller_users.doCreateUser);

        app.post('/techu_bank/v1/feedback', validate(validations.post_feedback), token.validate, controller_feedback.evalFeedback);

        app.get('/techu_bank/v1/ping', (req,res)=> {
            return res.send('<p>alive!!</p>');
        });

        // gestion de erores por defecto
        app.use(function (err, req, res, next) {
            // errores de validacion
            if (err instanceof validate.ValidationError) return res.status(err.status).json(err);

            // Otro tipo de errores 
            if (process.env.NODE_ENV !== 'production') {
                return res.status(500).send(err.stack);
            } else {
                return res.status(500);
            }
        });
    }
})



module.exports = app
