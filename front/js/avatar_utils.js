
var avatarStore = (function() {

  let s3;

  function init(config) {
    AWS.config.update({
      region: config.bucketRegion,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: config.IdentityPoolId
      })
    });

    s3 = new AWS.S3({
      apiVersion: "2006-03-01",
      params: { Bucket: config.bucketName }
    });

    return this;
  }

  function uploadImage(file) {
    let fileName = file.name;
    return new Promise(function(resolve, reject) {
      s3.upload(
        {
          Key: fileName,
          Body: file,
          ACL: "public-read"
        },
        function(err, data) {
          if (err) {
            reject(err.message);
          }
          resolve({
            path: data.Location,
            filename: data.Key
          });
        }
      );
    });
  }

  return {
    init,
    uploadImage
  };
})();
