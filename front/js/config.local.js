const _appconfig = {
  api_endpoint:
    "http://localhost:3000/techu_bank/V1",
  localization_endpoint: "https://api.ipdata.co",
  weather_endpoint: "http://api.openweathermap.org/data/2.5/weather",
  weather_apikey: "XXXXXXXXXX",
  weather_icon_url: "http://openweathermap.org/img/w/",
  avatarStore: {
    bucketName: "XXXXX",
    bucketRegion: "XXXXX",
    IdentityPoolId: "XXXXXXx"
  }
};
